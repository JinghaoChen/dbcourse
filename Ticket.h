#pragma once
class Ticket
{
public:
	int id;
	int sellingBy;
	bool isSold;

	Ticket(int id) {
		this->id = id;
		this->sellingBy = 0;
		this->isSold = false;
	}
};

