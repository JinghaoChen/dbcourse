﻿// MovieMFCDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MovieMFC.h"
#include "MovieMFCDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMovieMFCDlg 对话框



CMovieMFCDlg::CMovieMFCDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MOVIEMFC_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMovieMFCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, edit1);
	DDX_Control(pDX, IDC_EDIT2, edit2);
	DDX_Control(pDX, IDC_EDIT3, edit3);
	DDX_Control(pDX, IDC_TEXT1, txt1);
	DDX_Control(pDX, IDC_TEXT2, txt2);
}

BEGIN_MESSAGE_MAP(CMovieMFCDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMovieMFCDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CMovieMFCDlg 消息处理程序

BOOL CMovieMFCDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	/************************初始化START************************************/
	this->pEdit1 = (CEdit*)GetDlgItem(IDC_EDIT2);	// 绑定左侧文本框
	this->pEdit2 = (CEdit*)GetDlgItem(IDC_EDIT3);	// 绑定右侧文本框
	this->pTxt1 = (CStatic*)GetDlgItem(IDC_TEXT2);	// 绑定余票静态文本框
	this->threadIds.clear();						// 清空线程统计
	this->tickets.clear();							// 清空影票集合 
	this->ticketNums = 0;							// 总票数
	this->nowTickets = 0;							// 剩余票数
	edit1.SetWindowTextW(TEXT("100"));				//默认售卖100张票
	/************************初始化END**************************************/

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMovieMFCDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMovieMFCDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMovieMFCDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMovieMFCDlg::OnBnClickedButton1()
{
	/*****************************入参校验START****************************/
	CString str;
	edit1.GetWindowTextW(str);					// 获取输入框内容
	if (checkString(str)) {						// 校验
		this->ticketNums = _ttoi(str);			// 转换为数字并设置给总票数
		this->nowTickets = this->ticketNums;	// 同步给余量
	}
	else {
		this->ticketNums = 0;					// 出错就设置为0
		this->nowTickets = 0;
	}
	/******************************入参校验END*****************************/


	GetDlgItem(IDC_BUTTON1)->EnableWindow(false);//禁用button的点击，保证点击最多只能点击一次
	edit2.SetWindowTextW(TEXT(""));				// 清空文本，保证运行开始时为空
	edit3.SetWindowTextW(TEXT(""));
	initTicket();								// 实例化影票
	CWinThread* p1 = AfxBeginThread(SaleThread1, this);	// window1开始卖票
	CWinThread* p2 = AfxBeginThread(SaleThread2, this);	// window2开始卖票
}

UINT CMovieMFCDlg::SaleThread1(void* param)
{
	CMovieMFCDlg* dlg = (CMovieMFCDlg*)param;		// 获取实例
	int id = GetCurrentThreadId();					// 获取线程id用于注册
	dlg->ThreadStart(id);							// 注册线程，统计

	int num = dlg->ticketNums;						// 保存总票数
	int len = dlg->pEdit1->GetWindowTextLengthW();	// 获取文本框文本长度
	dlg->pEdit1->SetSel(len, len);					// 设置到文本最后，此后可追加文本
	CString str;

	for (int i = 0; i < num; i++) {					// 遍历所有影票是否可卖
		if (!dlg->tickets[i].isSold && dlg->tickets[i].sellingBy == 0) { // 未售、未被占，未售放前面可以提前结束判断
			dlg->tickets[i].sellingBy = id;			// 占有这张票
			Sleep(2);								// 缓冲，等待二次检查
			if (!dlg->tickets[i].isSold && dlg->tickets[i].sellingBy == id) { //二次检查，未售、还被自己占有
				dlg->tickets[i].isSold = true;		// 卖票
				str.Format(TEXT("No.1 window has sold a ticket: %d"), dlg->tickets[i].id); // 设置售卖信息
				dlg->pEdit1->ReplaceSel(str + TEXT("\r\n")); // 展示售卖信息
				dlg->updateText();					// 更新剩余票量
				Sleep(250);							// 等待下一位顾客买票（假设两位顾客卖票间隔为500ms）
			}
		}
	}

	dlg->ThreadEnd(id);								// 注销线程
	return 0;
}

UINT CMovieMFCDlg::SaleThread2(void* param)			// 各部分与SaleThread1相同，仅部分参数不同
{
	CMovieMFCDlg* dlg = (CMovieMFCDlg*)param;
	int id = GetCurrentThreadId();
	dlg->ThreadStart(id);

	int num = dlg->ticketNums;
	int len = dlg->pEdit2->GetWindowTextLengthW();
	dlg->pEdit2->SetSel(len, len);


	CString str;
	for (int i = 0; i < num; i++) {

		if (!dlg->tickets[i].isSold && dlg->tickets[i].sellingBy == 0) {
			dlg->tickets[i].sellingBy = id;
			Sleep(2);
			if (!dlg->tickets[i].isSold && dlg->tickets[i].sellingBy == id) {
				dlg->tickets[i].isSold = true;
				str.Format(TEXT("No.2 window has sold a ticket: %d"), dlg->tickets[i].id);
				dlg->pEdit2->ReplaceSel(str + TEXT("\r\n"));
				dlg->updateText();
				Sleep(100);							// 等待下一位顾客买票（假设两位顾客卖票间隔为100ms）
			}
		}
	}

	dlg->ThreadEnd(id);
	return 0;
}

void CMovieMFCDlg::ThreadStart(int id)
{
	this->threadIds.insert(id);	// 在threadIds中保存当前线程的id
}

void CMovieMFCDlg::ThreadEnd(int id)
{
	this->threadIds.erase(id);	// 从threadIds中去除当前线程的id
	if (this->threadIds.empty()) {	// 若此时没有还在运行中的线程，就恢复“开始卖票”button为可点击
		GetDlgItem(IDC_BUTTON1)->EnableWindow(true);
		this->saveData();
	}
}

bool CMovieMFCDlg::checkString(CString strMFC)
{
	std::string str;
	str = CT2A(strMFC.GetBuffer(0));	// CString转std::string
	std::string par{ "^[0-9]+$" };		// 正则模式串，纯数字
	std::regex re(par);
	bool retMatchStr = std::regex_match(str, re);	// 验证
	return retMatchStr;
}

void CMovieMFCDlg::initTicket()
{
	this->tickets.clear();	// 清空之前的实体
	for (int i = 1; i <= this->ticketNums; i++) {
		this->tickets.push_back(Ticket(i)); // 创建并保存实体
	}

	// shuffle
	std::random_shuffle(this->tickets.begin(), this->tickets.end());
}

void CMovieMFCDlg::updateText()
{
	while (isUsingUpdate) {	// 做线程同步，防止同时操作nowTickets使得结果不可用
		Sleep(2);				// 等待
	}
	isUsingUpdate = true;		// 加锁
	this->nowTickets--;			// 余量-1
	CString str;
	str.Format(TEXT("剩余票量：%d"), this->nowTickets);
	txt2.SetWindowTextW(str);	// 更新文本
	isUsingUpdate = false;		// 释放
}

void CMovieMFCDlg::saveData()
{
	//定义需要的变量
	CString textStr;
	std::string str;
	std::ofstream saveFile;
	//保存一号窗口
	saveFile.open("window1.txt");	// 打开文件
	edit2.GetWindowTextW(textStr);	// 获取内容
	str = CT2A(textStr.GetBuffer(0));	// CString转std::string
	saveFile << str;				// 写入数据
	saveFile.close();				// 关闭文件
	//保存二号窗口
	saveFile.open("window2.txt");
	edit3.GetWindowTextW(textStr);
	str = CT2A(textStr.GetBuffer(0));
	saveFile << str;
	saveFile.close();
}
