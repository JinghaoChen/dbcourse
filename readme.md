# 影院售票系统

## 背景

这个项目是“数据库实践”课程的一个小课设，是为“操作系统”课程而设立的实验。



## 使用技术

MFC、多线程



## 简介

该项目要求有两个售票窗口同时进行售票，使用多线程、并发控制等技术对两个售票窗口进行同步，保证电影票不重复售卖。





**一些说明：**

1.  运行时需要使用Visual Studio打开项目文件，再点击运行以运行结果，项目没有进行打包，如果需要，可以自行打包成exe或者其他形式。
2.  运行结果将自动保存到项目目录下的window1.txt和window2.txt，分别对应1号窗口和2号窗口的运行结果，可以在MovieMFCDlg.cpp文件中的saveData()方法中修改对应的目标文件，saveData()方法将在ThreadEnd()方法中调用。
3.  可以在MovieMFCDlg.cpp的两个方法：SaleThread1和SaleRhread2中修改Sleep的数值来控制两位售票员的售票速度。
4.  自认为注释比较详细，可以参考注释进行代码的理解与修改。
5.  项目有些粗糙，这个东西仅给了近一周的时间。



