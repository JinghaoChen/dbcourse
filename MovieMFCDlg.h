﻿
// MovieMFCDlg.h: 头文件
//

#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <regex>
#include <set>
#include <vector>
#include <algorithm>
#include "Ticket.h"

// CMovieMFCDlg 对话框
class CMovieMFCDlg : public CDialogEx
{
	// 构造
public:
	CMovieMFCDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MOVIEMFC_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();

	static UINT SaleThread1(void* param);
	static UINT SaleThread2(void* param);
	void ThreadStart(int id);
	void ThreadEnd(int id);

private:
	CEdit edit1;
	CEdit edit2;
	CEdit edit3;
	CEdit* pEdit1;
	CEdit* pEdit2;
	int ticketNums;
	std::vector<Ticket> tickets;
	std::vector <int>::iterator Iter;

	int nowTickets;
	std::set<int> threadIds;

	bool checkString(CString str);
	void initTicket();
	void updateText();
	void saveData();
	bool isUsingUpdate = false;
	CStatic txt1;
	CStatic txt2;
	CStatic* pTxt1;
};
